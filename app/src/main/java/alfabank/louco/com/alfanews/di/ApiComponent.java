package alfabank.louco.com.alfanews.di;

import javax.inject.Singleton;

import alfabank.louco.com.alfanews.mvp.model.api.ApiRetrofitConnect;
import alfabank.louco.com.alfanews.mvp.model.api.RequestServer;
import dagger.Component;

@Component(modules = {ApiRetrofitConnect.class})
@Singleton
public interface ApiComponent {
    void inject(RequestServer requestServer);
}
