package alfabank.louco.com.alfanews.di;

import alfabank.louco.com.alfanews.mvp.model.api.ApiRetrofitConnect;

public class MyApiComponent {

    private static ApiComponent apiComponent = null;

    public static ApiComponent getApiComponent(){
        if(apiComponent == null){
            apiComponent = buildComponent();
        }
        return apiComponent;
    }

    private static ApiComponent buildComponent() {

        return DaggerApiComponent.builder()
                .apiRetrofitConnect(new ApiRetrofitConnect())
                .build();
    }
}
