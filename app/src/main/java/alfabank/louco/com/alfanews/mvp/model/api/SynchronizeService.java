package alfabank.louco.com.alfanews.mvp.model.api;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import alfabank.louco.com.alfanews.mvp.model.db.ContentNewsDao;
import alfabank.louco.com.alfanews.mvp.model.db.NewsDatabase;

public class SynchronizeService extends Service {

    private NewsDatabase newsDatabase;
    private ContentNewsDao newsDao;

    public static final int notify = 300000;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        newsDatabase = NewsDatabase.getInstanceNews(getBaseContext());
        newsDao = newsDatabase.getContentNewsDao();

        if (mTimer != null)
            mTimer.cancel();
        else
            mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimeDisplay(), 0, notify);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();
        Log.d("Louco", "Service is Destroyed");
    }

    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            mHandler.post(() -> {
                Log.d("Louco", "Run");
                new RequestServer.Builder().getServer(new SubscribeNews(newsDao));
            });
        }
    }
}
