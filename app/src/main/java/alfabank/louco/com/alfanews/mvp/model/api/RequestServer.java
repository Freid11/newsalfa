package alfabank.louco.com.alfanews.mvp.model.api;

import javax.inject.Inject;

import alfabank.louco.com.alfanews.di.MyApiComponent;
import alfabank.louco.com.alfanews.mvp.model.Object.Rss;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class RequestServer {

    private static final int SUBTYPE = 1;
    private static final int CATEGORY = 2;
    private static final int CITY = 23;

    @Inject
    Retrofit retrofit;

    private RequestServer() {
        MyApiComponent.getApiComponent().inject(RequestServer.this);
    }

   public static class Builder{

       private RequestServer requestServer;

       public Builder() {
           requestServer = new RequestServer();
       }

       public void getServer(DisposableObserver<Rss> disposableObserver){
           requestServer.retrofit
                   .create(NewsApi.class)
                   .getNews(SUBTYPE, CATEGORY, CITY)
                   .subscribeOn(Schedulers.io())
                   .observeOn(AndroidSchedulers.mainThread())
                   .subscribe(disposableObserver);
       }

       public RequestServer create(){
           return requestServer;
       }
   }

}
