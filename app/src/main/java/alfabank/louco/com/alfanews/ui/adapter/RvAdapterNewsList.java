package alfabank.louco.com.alfanews.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import alfabank.louco.com.alfanews.R;
import alfabank.louco.com.alfanews.mvp.model.Object.Item;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RvAdapterNewsList extends RecyclerView.Adapter<RvAdapterNewsList.ViewHolder> {

    private static final String FORMAT_DATE_SERVER = "EEE, dd MMM yyyy HH:mm:ss z";
    private static final String FORMAT_DATE_VIEW = "dd MMM yyyy HH:mm";
    private List<Item> mItems = new ArrayList<>();
    private OnListenerClickNews mOnListenerClickNews;
    private SimpleDateFormat simpleDateFormat;
    private Context context;

    public RvAdapterNewsList(OnListenerClickNews onListenerClickNews) {
        this.mOnListenerClickNews = onListenerClickNews;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_rv_news, parent, false);
        simpleDateFormat = new SimpleDateFormat(FORMAT_DATE_SERVER, Locale.ENGLISH);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Item itemNews = mItems.get(position);
        holder.bind(itemNews, position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void addItemNews(List<Item> items){
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    public void reloadItemNews(List<Item> items){
        mItems = items;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView textTitle;

        @BindView(R.id.tv_date)
        TextView textDate;

        @BindView(R.id.iv_favorite)
        ImageView imageFavorite;

        private int position;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(view -> mOnListenerClickNews.onClick(position));
        }

        void bind(Item item, int position){
            this.position = position;

            if(item.getFavorite()){
                imageFavorite.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite_acent));
            }else{
                imageFavorite.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite_border_acent));
            }

            textTitle.setText(Html.fromHtml(item.getTitle()));
            try {
                Date date = simpleDateFormat.parse(item.getPubDate());
                simpleDateFormat.applyLocalizedPattern(FORMAT_DATE_VIEW);
                textDate.setText(simpleDateFormat.format(date));

            } catch (ParseException e) {
                textDate.setText(item.getPubDate());
            }
        }
    }

    public interface OnListenerClickNews{
        void onClick(int position);
    }
}
