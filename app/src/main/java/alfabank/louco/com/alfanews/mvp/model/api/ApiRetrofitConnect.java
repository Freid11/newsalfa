package alfabank.louco.com.alfanews.mvp.model.api;

import javax.inject.Singleton;

import alfabank.louco.com.alfanews.BuildConfig;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

@Module
public class ApiRetrofitConnect {

    private static final String URL = BuildConfig.URL;
    private static Retrofit retrofit;

    @Provides
    @Singleton
    public static Retrofit instanceRetrofit(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(SimpleXmlConverterFactory.create())
                    .build();
        }

        return retrofit;
    }
}
