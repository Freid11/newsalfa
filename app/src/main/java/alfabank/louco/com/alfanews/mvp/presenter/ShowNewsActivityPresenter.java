package alfabank.louco.com.alfanews.mvp.presenter;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import alfabank.louco.com.alfanews.mvp.model.Object.Item;
import alfabank.louco.com.alfanews.mvp.model.db.ContentNewsDao;
import alfabank.louco.com.alfanews.mvp.model.db.NewsDatabase;
import alfabank.louco.com.alfanews.mvp.view.ShowNewsActivityView;

@InjectViewState
public class ShowNewsActivityPresenter extends MvpPresenter<ShowNewsActivityView>{

    private NewsDatabase newsDatabase;
    private ContentNewsDao newsDao;

    private LiveData<List<Item>> listLiveData;
    private int position;

    public ShowNewsActivityPresenter() {

    }

    public void showNews(Activity activity){
        newsDatabase = NewsDatabase.getInstanceNews(activity);
        newsDao = newsDatabase.getContentNewsDao();

        listLiveData = NewsListPresenter.getTypeListNews(newsDao);
        listLiveData.observe((LifecycleOwner) activity, items -> {
            getViewState().ShowNews(items, this.position);
            listLiveData.removeObservers((LifecycleOwner) activity);
        });
    }

    public void setPositionViewState(int position) {
        this.position = position;

    }

}
