package alfabank.louco.com.alfanews.mvp.presenter;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import alfabank.louco.com.alfanews.mvp.model.api.RequestServer;
import alfabank.louco.com.alfanews.mvp.model.api.SubscribeNews;
import alfabank.louco.com.alfanews.mvp.model.api.SynchronizeService;
import alfabank.louco.com.alfanews.mvp.model.db.ContentNewsDao;
import alfabank.louco.com.alfanews.mvp.model.db.NewsDatabase;
import alfabank.louco.com.alfanews.mvp.view.SplashView;

@InjectViewState
public class SplashPresenter extends MvpPresenter<SplashView> {

    private static final String KEY_FIRST_ENTER = "first_enter";
    private static final Boolean DEFAULT = true;

    private NewsDatabase newsDatabase;
    private ContentNewsDao newsDao;

    public SplashPresenter() {
    }

    public void checkNews(final Activity activity) {

        newsDatabase = NewsDatabase.getInstanceNews(activity);
        newsDao = newsDatabase.getContentNewsDao();

        // очистить базу
        newsDao.clearBase();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);

        if (isFirstEnter(sharedPreferences)) {
            new RequestServer.Builder().getServer(new SubscribeNews(newsDao));
            markFirstEnter(sharedPreferences);
        } else {
            activity.startService(new Intent(activity, SynchronizeService.class));
        }

        getViewState().goToListNewsActivity();

    }

    private void markFirstEnter(SharedPreferences sharedPreferences) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_FIRST_ENTER, false);
        editor.apply();
    }

    private boolean isFirstEnter(SharedPreferences sharedPreferences) {
        return sharedPreferences.getBoolean(KEY_FIRST_ENTER, DEFAULT);
    }
}
