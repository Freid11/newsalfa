package alfabank.louco.com.alfanews.mvp.model.Object;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
@Entity
public class Item implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @Element(required = false)
    private String guid;

    @Element(required = false)
    private String pubDate;

    @Element(required = false)
    private String title;

    @Element(required = false,data=true, name="description")
    private String description;

    @Element(required = false)
    private String link;

    private Boolean favorite = false;

    public Item() {
    }

    protected Item(Parcel in) {
        id = in.readInt();
        guid = in.readString();
        pubDate = in.readString();
        title = in.readString();
        description = in.readString();
        link = in.readString();
        byte tmpFavorite = in.readByte();
        favorite = tmpFavorite == 0 ? null : tmpFavorite == 1;
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public String getGuid() {
        return guid;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    @Override
    public String toString()
    {
        return "Class Item[guid = "+guid+", pubDate = "+pubDate+", title = "+title+", description = "+description+", link = "+link+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(guid);
        dest.writeString(pubDate);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(link);
        dest.writeByte((byte) (favorite == null ? 0 : favorite ? 1 : 2));
    }
}


