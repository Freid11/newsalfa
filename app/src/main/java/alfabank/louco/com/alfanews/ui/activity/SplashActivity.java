package alfabank.louco.com.alfanews.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import alfabank.louco.com.alfanews.mvp.model.api.SynchronizeService;
import alfabank.louco.com.alfanews.mvp.presenter.SplashPresenter;
import alfabank.louco.com.alfanews.mvp.view.SplashView;

public class SplashActivity extends MvpAppCompatActivity implements SplashView {

    @InjectPresenter
    SplashPresenter splashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        splashPresenter.checkNews(this);
    }

    @Override
    public void goToListNewsActivity() {
        Intent intent = new Intent(this, NewsListActivity.class);
        startActivity(intent);
        Log.d("Louco", "goToListNewsActivity");
        finish();
    }
}
