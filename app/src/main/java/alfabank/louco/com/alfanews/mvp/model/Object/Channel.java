package alfabank.louco.com.alfanews.mvp.model.Object;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

@Root
public class Channel
{
    @Element(required = false)
    private String pubDate;

    @Element(required = false)
    private String title;

    @Element(required = false)
    private String managingEditor;

    @Element(required = false)
    private String description;

    @Element(required = false)
    private String docs;

    @Element(required = false)
    private String link;

    @ElementList(required = false, inline = true)
    private ArrayList<Item> item;

    @Element(required = false)
    private String language;

    @Element(required = false)
    private String webMaster;

    public String getPubDate ()
    {
        return pubDate;
    }

    public String getTitle() {
        return title;
    }

    public String getManagingEditor() {
        return managingEditor;
    }

    public String getDescription() {
        return description;
    }

    public String getDocs() {
        return docs;
    }

    public String getLink() {
        return link;
    }

    public ArrayList<Item> getItem() {
        return item;
    }

    public String getLanguage() {
        return language;
    }

    public String getWebMaster() {
        return webMaster;
    }

    @Override
    public String toString()
    {
        return "Class Channel [pubDate = "+pubDate+", title = "+title+", managingEditor = "+managingEditor+", description = "+description+", docs = "+docs+", link = "+link+", item = "+item+", language = "+language+", webMaster = "+webMaster+"]";
    }
}

