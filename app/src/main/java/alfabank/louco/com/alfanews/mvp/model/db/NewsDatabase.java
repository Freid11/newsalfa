package alfabank.louco.com.alfanews.mvp.model.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import alfabank.louco.com.alfanews.mvp.model.Object.Item;

@Database(entities = {Item.class}, version = 3, exportSchema = false)
public abstract class NewsDatabase extends RoomDatabase {
    private static final String NEWS_DATABASE = "news";
    private static final Object LOCK = new Object();
    private static NewsDatabase newsDatabase;

    public static NewsDatabase getInstanceNews(Context context){
        if(newsDatabase == null){
            synchronized (LOCK){
                newsDatabase = Room.databaseBuilder(context,
                        NewsDatabase.class, NEWS_DATABASE)
                        .fallbackToDestructiveMigration()
                        .build();
            }
        }
        return newsDatabase;
    }

    public ContentNewsDao getContentNewsDao(){
        return new ContentNewsDao(getNewsDao());
    }

    public abstract NewsDao getNewsDao();
}
