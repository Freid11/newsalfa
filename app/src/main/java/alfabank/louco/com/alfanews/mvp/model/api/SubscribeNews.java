package alfabank.louco.com.alfanews.mvp.model.api;

import android.util.Log;

import alfabank.louco.com.alfanews.mvp.model.Object.Rss;
import alfabank.louco.com.alfanews.mvp.model.db.ContentNewsDao;
import io.reactivex.observers.DisposableObserver;

public class SubscribeNews extends DisposableObserver<Rss> {

    private static final String LOG_TAG = "Louco";

    private ContentNewsDao contentNewsDao;

    public SubscribeNews(ContentNewsDao contentNewsDao) {
        this.contentNewsDao = contentNewsDao;
    }

    @Override
    public void onNext(Rss rss) {
        if (rss != null) {
            contentNewsDao.insertList(rss.getChannel().getItem());
        } else {
            onError(new Throwable("null"));
        }
    }

    @Override
    public void onError(Throwable e) {
        Log.e(LOG_TAG, e.getMessage());
    }

    @Override
    public void onComplete() {
        Log.d(LOG_TAG, "onComplete");
    }
}
