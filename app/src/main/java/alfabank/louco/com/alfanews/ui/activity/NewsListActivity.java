package alfabank.louco.com.alfanews.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import alfabank.louco.com.alfanews.R;
import alfabank.louco.com.alfanews.mvp.model.Object.Item;
import alfabank.louco.com.alfanews.mvp.presenter.NewsListPresenter;
import alfabank.louco.com.alfanews.mvp.view.NewsListView;
import alfabank.louco.com.alfanews.ui.adapter.RvAdapterNewsList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsListActivity extends MvpAppCompatActivity implements NewsListView{

    @BindView(R.id.srl_news)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.rv_news)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.fab_news_list)
    FloatingActionButton floatingActionButton;

    @InjectPresenter
    NewsListPresenter newsListPresenter;

    private RvAdapterNewsList adapterNewsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);
        ButterKnife.bind(this);

        toolbar.setTitle("News");
        setSupportActionBar(toolbar);

        newsListPresenter.getNewsList(this);

        adapterNewsList = new RvAdapterNewsList(position -> {
            newsListPresenter.onClickNews(position);
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapterNewsList);

        swipeRefreshLayout.setOnRefreshListener(() ->
            newsListPresenter.getNewsList(this)
        );

        floatingActionButton.setOnClickListener(view ->{
            newsListPresenter.floatButtonAction(this);
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showNews(int position) {
        Intent intent = new Intent(this, ShowNewsActivity.class);
        intent.putExtra(ShowNewsActivity.KEY_POSITION_NEWS, position);
        startActivity(intent);
    }

    @Override
    public void reloadNewsList(List<Item> itemList) {
        Log.d("Louco", "showNews");
        adapterNewsList.reloadItemNews(itemList);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setFloatButtonIcon(Boolean typeList) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        if(typeList){
            floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_all_list));
        }else{
            floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite));
        }
    }

}
