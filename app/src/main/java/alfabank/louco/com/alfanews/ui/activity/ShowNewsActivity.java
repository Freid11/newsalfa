package alfabank.louco.com.alfanews.ui.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import alfabank.louco.com.alfanews.R;
import alfabank.louco.com.alfanews.mvp.model.Object.Item;
import alfabank.louco.com.alfanews.mvp.presenter.ShowNewsActivityPresenter;
import alfabank.louco.com.alfanews.mvp.view.ShowNewsActivityView;
import alfabank.louco.com.alfanews.ui.adapter.VpAdapterNews;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowNewsActivity extends MvpAppCompatActivity implements ShowNewsActivityView{

    public static final String KEY_POSITION_NEWS = "position";
    public static final int DEFAULT_VALUE = 0;

    private VpAdapterNews vpAdapterNews;

    @InjectPresenter
    ShowNewsActivityPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.vp_news)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_news);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(savedInstanceState == null) {
            int positionViewPage = getIntent().getIntExtra(KEY_POSITION_NEWS, DEFAULT_VALUE);
            presenter.setPositionViewState(positionViewPage);
        }

        presenter.showNews(this);
    }

    @Override
    public void ShowNews(List<Item> itemList, int position) {
        Log.d("Louco", "ShowNews");
        vpAdapterNews = new VpAdapterNews(getSupportFragmentManager(), itemList);
        viewPager.setAdapter(vpAdapterNews);
        viewPager.setCurrentItem(position);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.setPositionViewState(viewPager.getCurrentItem());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
