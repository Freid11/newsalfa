package alfabank.louco.com.alfanews.mvp.view

import com.arellomobile.mvp.MvpView

interface SplashView : MvpView{
    /** переход в новое активити */
    fun goToListNewsActivity()
}